package com.sanjay.spring.batch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CUSTOMER_INFO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Policy {
	@Id
	@Column(name = "hcl_policies_sf")
	private int sfid;
	@Column(name = "policy_id")
	private String policyid;
	@Column(name = "sf_name")
	private String sfname;
	@Column(name = "sf_desc")
	private String sfdesc;
		
}
