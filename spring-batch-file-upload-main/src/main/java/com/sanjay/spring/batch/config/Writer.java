package com.sanjay.spring.batch.config;


import java.util.List;
import com.sanjay.spring.batch.entity.Policy;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sanjay.spring.batch.repository.PolicyRepository;

@Component
public class Writer implements ItemWriter<Policy> {

    @Autowired
    private PolicyRepository policyrepository;

	@Override
	public void write(List<? extends Policy> items) throws Exception {
		System.out.println("Writer Thread" + Thread.currentThread().getId());
		policyrepository.saveAll(items);
		// TODO Auto-generated method stub
		
	}


    
//    public void write(List<? extends Policy> list) throws Exception {
//        System.out.println("Writer Thread "+Thread.currentThread().get
//        repository.saveAll(list);
//    }
}
