package com.sanjay.spring.batch.config;

import org.springframework.batch.item.ItemProcessor;

import com.sanjay.spring.batch.entity.Policy;

public class Processor implements ItemProcessor<Policy, Policy> {
    @Override
    public Policy process(Policy policy) {
        int age = Integer.parseInt(policy.getPolicyid());
		return policy;
        
    }
}
