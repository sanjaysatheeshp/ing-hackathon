package com.sanjay.spring.batch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sanjay.spring.batch.entity.Policy;

public interface PolicyRepository extends JpaRepository<Policy,Integer> {

	
}
